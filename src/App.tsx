import * as React from 'react';
import './App.css';
import CampaignGantt from "./components/CampaignGantt";
import {Campaign, dhtmlxData, Message} from "./types";
import ChangeLog from "./components/ChangeLog";
import axios from "axios";
import {dhtmlToCampaign} from "./helper";

const apiClient = function() {
    const params = {
        headers: {
            'Content-Type': 'application/json ',
        },
        baseURL: "http://localhost:51965/api"
    };
    return axios.create(params);
};

export interface Props { }

interface State {
    campaigns: Campaign[],
    messages: Message[],
    loading: boolean,
}

class App extends React.Component<Props,State> {
    state: State = {
        campaigns:[],
        messages:[],
        loading: true,
    };

    componentDidMount() {
        apiClient().get<Campaign[]>("/Campaign")
            .then(response => {
                this.setState({
                    campaigns:response.data,
                    loading: false
                });
                console.info("app get initial data - response: ", response.data)
            })
            .catch(error => {
                console.info("app get initial data - error: ", error)
            })

    }

    /*
        postNewCampaign returns an AxiosPromise
        the data flow of react app should be from top component
        down to child components. My first approach was to have the
        data in the main App state and pass it down to the dhtmlx
        component wrapper (CampaignGantt). Where the data gets parsed
        every prop update (componentDidUpdate), but dhtmlx has its own
        storage and id creation. To keep the App state campaigns array in
        sync with the dhtmlx storage the response gets passed to the Wrapper,
        where the id of the campaign gets changed.
     */
    postNewCampaign = (id:number, dhtmlxCampaign:dhtmlxData) => {
        const campaign:Campaign = dhtmlToCampaign(dhtmlxCampaign);
        this.addMessage(id,"added",dhtmlxCampaign);
        const axiosPromiss = apiClient().post("/Campaign", {
            Title: campaign.Title,
            StartDate: campaign.StartDate,
            EndDate: campaign.EndDate,
            Color: campaign.Color
        });

        axiosPromiss.then(response => {
            console.info("post response: ",response);
            this.setState({
               campaigns: [...this.state.campaigns,response.data]
            })
        });

        return axiosPromiss;
    };

    logTaskUpdate = (id:number, mode:string, dhtmlxCampaign:dhtmlxData) => {

        this.addMessage(id,mode,dhtmlxCampaign);
        //there is a bug when a campaign is dragged very small and
        //the backend returns an error (start end span below 1 hour)
        //then dhtmlx still updates the campaign in its own storage
        //this could be solved by passing the result down to the wrapper
        //but i wont bother fixing this. dhtmlx should not be used with react
        switch(mode){
            case 'updated': {
                const campaignToUpdate: Campaign = dhtmlToCampaign(dhtmlxCampaign);
                apiClient().put("/Campaign/"+campaignToUpdate.Id, campaignToUpdate)
                    .then(response => {
                        console.info("put response:", response);
                        this.setState({
                            campaigns: this.state.campaigns.map(item =>
                                item.Id === campaignToUpdate.Id ? campaignToUpdate : item)
                        });
                    })
                    .catch(error => {
                        console.info("put response - error:", error);
                        //Error Handling
                    });
                break;
            }
            case 'deleted': {
                apiClient().delete("/Campaign/"+id)
                    .then(response => {
                        console.info("delete response - response:", response);
                        this.setState({
                            campaigns: this.state.campaigns.filter((item => item.Id !== id))
                        })
                    }).catch( error => {
                        console.info("delete response - error:", error);
                    });
                break;
            }
            default:
                break;
        }

    };

    public render() {
        return (
            <div className="App">
                <div className="gantt-container">
                    {   // unfortunately dhmtlxgantt needs to parse the given data and it can't be done every
                        // prop update (see long comment above) the wrapper needs to wait for the initial data
                        // to be fetched since its not refreshing once the data arrives
                        this.state.loading ? <span>LOADING..</span> :
                            <CampaignGantt
                                campaigns={this.state.campaigns}
                                onTaskUpdated={this.logTaskUpdate}
                                onTaskAdd={this.postNewCampaign}
                            />
                    }
                </div>
                <ChangeLog messages={this.state.messages}/>
            </div>
        );
    }

    addMessage(id:number, mode:string, dhtmlxCampaign:dhtmlxData) {
        let text: string = dhtmlxCampaign && dhtmlxCampaign.text ? ` (${dhtmlxCampaign.text})`: '';
        let startDate: string = dhtmlxCampaign && dhtmlxCampaign.start_date ? dhtmlxCampaign.start_date.toString() : '';
        let content:string = `Campaign ${mode}: ${id} ${text} start date: ${startDate}`;

        //put new message first in Array
        let messages: Message[] = this.state.messages.slice();
        let prevKey: number = messages.length ? messages[0].key: 0;
        messages.unshift({key: prevKey + 1, content: content});
        if(messages.length > 40){
            messages.pop();
        }

        this.setState({messages: messages});
    }
}

export default App;
