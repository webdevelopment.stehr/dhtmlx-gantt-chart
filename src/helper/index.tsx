import * as moment from "moment";
import {Campaign, dhtmlxData, dhtmlxDataContainer} from "../types";

const getIsoDateString = (date:Date) => {
    return moment(date).format()
};

const addDays = (date:Date, days:number) => {
    let result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
};

const dateDif = (start:Date, end:Date) => {
    let timeDiff = Math.abs(end.getTime() - start.getTime());
    return Math.ceil(timeDiff / (1000 * 3600 * 24));
};

// converts from interface Campaign to interface dhtmlxData
// called before the obj is send to backend
// converts Dates to iso string
// there is probably a better solution (interface/types/classes constructors?)
export const dhtmlToCampaign = (dhtmlxCampaign: dhtmlxData) => {
    return {
        Id: dhtmlxCampaign.id,
        Title: dhtmlxCampaign.text,
        StartDate: getIsoDateString(dhtmlxCampaign.start_date),
        EndDate: getIsoDateString(addDays(new Date(dhtmlxCampaign.start_date), dhtmlxCampaign.duration)),
        Color: dhtmlxCampaign.color
    }
};

//converts from interface dhtmlxData to interface Campaign
export const campaignToDhtmlx = (campaign:Campaign) => {
    return {
        id: campaign.Id,
        text: campaign.Title,
        start_date: new Date(campaign.StartDate),
        duration: dateDif(new Date(campaign.EndDate), new Date(campaign.StartDate)),
        color: campaign.Color
    }
};

//converts a whole array of Campaign to dhtmlxData
export const campaignsToDhtmlxData = (campaigns:Campaign[]) => {
    let data:dhtmlxDataContainer;
    data = {
        data:
            campaigns.map(campaign => {
                return campaignToDhtmlx(campaign)})
    };
    return data;
};