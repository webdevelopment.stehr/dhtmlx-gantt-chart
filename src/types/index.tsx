
//TODO better using types? according to:
// https://medium.com/@martin_hotell/interface-vs-type-alias-in-typescript-2-7-2a8f1777af4c

//clients representation of api response data
export interface Campaign {
    Id: number;
    Title: string;
    StartDate: string;
    EndDate: string;
    Color: string;
}

//interface for single dhtmlxgantt data element
export interface dhtmlxData {
    id: number,
    text: string,
    start_date: Date,
    duration: number,
    color: string
}

//interface for dhtmlxgantt "data" obj containing an array -data- of dhtmlxData elements
export interface dhtmlxDataContainer {

    data:dhtmlxData[]
}

export interface Message {
    key: number;
    content: string;
}