import * as React from "react"
import {Message} from "../types";

interface Props {
    messages: Message[]
}

class ChangeLog extends React.Component<Props> {
    render() {
        return (
            <div className="change-log">
                <ul>
                    {this.props.messages.map((message:Message) => {
                        return <li key={message.key}>{message.content}</li>
                    })}
                </ul>
            </div>
        )
    }
}

export default ChangeLog;