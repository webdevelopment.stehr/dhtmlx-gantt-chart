import * as React from "react"
import CampaignListElement from "./CampaignListElement";
import { Campaign } from "../types";

export interface Props {
    campaigns: Campaign[];
}

class CampaignList extends React.Component<Props> {
    constructor(props:Props) {
        super(props);
    }

    componentDidMount() {

    }

    render(){
        return (
            <div>
                { this.props.campaigns.map(campaign => <CampaignListElement key={campaign.Id} campaign={campaign}/>) }
            </div>
        )
    }
}

export default CampaignList