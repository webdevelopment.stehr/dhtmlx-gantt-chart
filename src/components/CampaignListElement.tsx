import * as React from "react"
import {Campaign} from "../types";

export interface Props {
    campaign: Campaign;
}

class CampaignListElement extends React.Component<Props> {
    constructor(props:Props) {
        super(props);
    }

    render () {
        return (
            <div>
                {this.props.campaign.Title}
            </div>
        )
    }

}

export default CampaignListElement;