import * as React from 'react';
import 'dhtmlx-gantt';
import 'dhtmlx-gantt/codebase/dhtmlxgantt.css';
import {Campaign, dhtmlxData} from "../types";
import {AxiosPromise} from "axios";
import {campaignsToDhtmlxData} from "../helper";

interface Props {
    campaigns: Campaign[];
    onTaskUpdated(id:number, mode:string, campaign?:dhtmlxData):void;
    onTaskAdd(id:number, campaign?:dhtmlxData):AxiosPromise;
}
interface State { }

class CampaignGantt extends React.Component<Props, State> {
    public ganttContainer: HTMLDivElement;

    constructor(props:Props) {
        super(props);
    }

    componentDidMount() {
        gantt.attachEvent('onBeforeTaskAdd', (id, campaign) => {
            if(this.props.onTaskAdd)
                this.props.onTaskAdd(id, campaign).then(response => {
                    //on successful Promise the updated campaign's id
                    //gets changed to the id created by backend:
                    gantt.changeTaskId(id, response.data.Id);
                }).catch(error => {
                    console.info("post response - error: ", error)
                })
        });

        gantt.attachEvent('onAfterTaskUpdate', (id, campaign) => {
            if(this.props.onTaskUpdated) {
                this.props.onTaskUpdated(id, 'updated', campaign);
            }
        });

        gantt.attachEvent('onBeforeTaskDelete', (id) => {
            if(this.props.onTaskUpdated) {
                this.props.onTaskUpdated(id, 'deleted');
            }
        });

        //enable vertical auto resize
        gantt.config.autosize = "y";
        //disable links between campaigns for now
        gantt.config.drag_links = false;
        //disable campaign progress slider
        gantt.config.drag_progress = false;

        gantt.config.date_grid = "%Y-%m-%d";

        gantt.init(this.ganttContainer);

        gantt.parse(campaignsToDhtmlxData(this.props.campaigns));
    }

    componentDidUpdate() {
        //probably useless
        gantt.render();
    }

    render() {
        return (
            <div
                ref={(input) => {this.ganttContainer = input ? input : this.ganttContainer }}
                style={{width: '100%', height: '100%'}}
            />
        );
    }
}

export default CampaignGantt